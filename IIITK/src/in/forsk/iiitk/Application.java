package in.forsk.iiitk;

import org.acra.annotation.ReportsCrashes;

import android.util.DisplayMetrics;

@ReportsCrashes(formKey = "", // This is required for backward compatibility but
								// not used
formUri = "https://collector.tracepot.com/3553b3fd")
public class Application extends android.app.Application {

	public static int mDeviceWidth = 0;
	public static int mDeviceHeight = 0;

	@Override
	public void onCreate() {
		super.onCreate();
/*
		AQUtility.setDebug(false);

		ACRA.init(this);
*/
		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
		mDeviceWidth = displayMetrics.widthPixels;
		mDeviceHeight = displayMetrics.heightPixels;
	}

}
