package in.forsk.iiitk;


///      done by ankit kaushik

public class _2013KUCP1023_Time_Table_Wrapper {

	 String time;
	    String batch;
	    String date;
	    String faculty;

	    public String getDate() {
	        return date;
	    }

	    public void setDate(String date) {
	        this.date = date;
	    }



	    public String getTime() {
	        return time;
	    }

	    public void setTime(String time) {
	        this.time = time;
	    }



	    public String getBatch() {
	        return batch;
	    }

	    public void setBatch(String batch) {
	        this.batch = batch;
	    }



	    public String getFaculty() {
	        return faculty;
	    }

	    public void setFaculty(String faculty) {
	        this.faculty = faculty;
	    }


	}
